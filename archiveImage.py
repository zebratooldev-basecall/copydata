#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
import glob
import shutil

# import json
# import pprint
# import cPickle as pickle

###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2015-04-03'
changelog = '''
            1.1.0 2016.08.25 change archiveImage to make it also apply to toColor's image 
            '''
###### Usage
usage = '''

     Version %s  by Vincent-Li  %s

     Usage: %s <inputDir> <outputDir> <fov> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################


##########################################################################
############################  BEGIN Function  ############################
##########################################################################
def archiveImage(inputDir, outputDir, fov):
    hasImages = False
    fovList = fov.strip().split(',')
    temp = []
    cycle_list = []
    dirList = os.listdir(inputDir)
    for d in dirList:
        if len(d) == 4 and d[0] in ['S', 'C']:
            temp.append(d)
    for d in temp:
        if os.path.isdir(os.path.join(inputDir,d)):
            cycle_list.append(os.path.join(inputDir,d))
    for cyclePath in cycle_list:
        cycle = os.path.basename(cyclePath)
        if cycle == 'Cleavge':
            continue
        outCyclePath = os.path.join(outputDir, cycle)
        if not os.path.exists(outCyclePath):
            os.makedirs(outCyclePath)
        for f in fovList:
            fovFiles = glob.glob(os.path.join(cyclePath, "*%s*.tif" % f))
            if len(fovFiles) > 0:
                hasImages = True
                for x in fovFiles:
                    fb = os.path.basename(x)
                    shutil.copyfile(x, os.path.join(outCyclePath, fb))
            else:
                pass
    return hasImages

######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (inputDir, outputDir, fov) = args

    ############################# Main Body #############################
    archiveImage(inputDir, outputDir, fov)


#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################
