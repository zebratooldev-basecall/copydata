copyData
========

Upload fastq and other files to cluster

Usage
-----

在linux环境中使用./make 即会生成一个以当前时间命名的目录，可以作为发布版本使用

File Meaning
------------

    software 目录， 包含win下打包文件的工具tar
    cwRsync 目录， 包含win下的ssh与rsync工具，其中有三个ini配置文件
                  分别是0.3.8 0.3.9 以及不带后缀的默认使用的配置文件
                  对应不同的basecall版本信息
    archiveImage.py  拷贝图片，可以copy fourColor和towColor两种类型的图片 
    copyData.py  数据上传的主脚本
    uploadData.py  在copyData.py的基础上修改出来的用于zebraTools的版本
                    功能一样
                  
数据上传配置
------------

请参考共享文档：https://onedrive.live.com/edit.aspx?resid=614E96D72C068FB2%21467&id=documents&authkey=!AMpv4gzLHxKeyuk&wd=pid$459489750$
