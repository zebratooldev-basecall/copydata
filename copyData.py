
#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
reload(sys)
sys.setdefaultencoding('utf8')

import subprocess
import shutil
import datetime
import glob

from archiveImage import archiveImage
###### Document Decription
''' When CaseCall is All Finished, copy Data to cluster '''

###### Version and Date
prog_version = '1.1.0'

prog_date = '2015-10-21' 

changelog = '''
            1.1.0  Add minUploadCycle, if read1's length less than it ,then don't upload

            1.1.1   11.9    Copy FileName.txt to cluster, then get fov's max value of Col and Row

            1.2.0   11.11   Add optional parameter for copying images

            1.3.0   2015.11.13  Add performance dir

            1.3.1   2015.11.19  Fix codes that when read1 and read2's lenght all less than 25, then dont upload

            2.0.0   2015.11.20  Refactor the codes to make the program more clear logic

            2.1.0   2015.11.25  Fix a bug that tar images

            2.2.0   2015.11.27  Change subprocess.Popen to os.popen that zebraTools can slove black box flash

            2.3.0   2015.12.18  Fix a bug that upload error in production machine.

            2.4.0   2016.08.25  add judgment line to decide reference by lane ranter than by slide
            '''
###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <dataInfoFile> <configFile> <laneInfoFile> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable
# rsync = os.path.join(currentDir, 'cwRsync', 'rsync.exe')
# ssh = os.path.join(currentDir, 'cwRsync', 'ssh.exe')

#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class CopyData(object):
    def __init__(self, dataInfoFile, configFile, laneInfoFile, upload=True, searchDir=False):
        self.dataInfoFile = dataInfoFile
        self.configFile = configFile
        self.laneInfoFile = laneInfoFile
        self.upload = upload
        self.searchDir = searchDir

        self.configDict = {}
        self.dataInfoDict = {}
        self.laneInfoDict = {}

        self.uploadPara = {}
        self.minUploadCycle = 25

        self.rsync = 'rsync'
        self.rsyncPara = '-rlv --exclude Intensities --chmod=Fu=rw,Du=rwx,Fg=r,Dg=rx,Fo=,Do='
        self.ssh = 'ssh'

    def readConfig(self):
        # Get config from C:\cwRsync\uploadConfig.ini
        import ConfigParser
        config = ConfigParser.ConfigParser()
        if os.path.exists(self.configFile):
            config.read(self.configFile)
            for k in ['name', 'directoryPermission', 'filePermission', 'uploadFq', 'uploadImage',
                        'uploadPerformance', 'mapping', 'imageFov', 'select']:
                self.configDict[k] = config.get('default', k)
            if self.configDict['select'] == 'remote':
                for k in ['host', 'address', 'infoAddress']:
                    self.configDict[k] = config.get('remote', k)
            elif self.configDict['select'] == 'local':
                for k in ['address', 'infoAddress']:
                    self.configDict[k] = config.get('local', k)
            return self.configDict
        else:
            print 'Cant not find %s.' % (self.configFile)
            return False

    def getDataInfo(self):
        # Get data about fq and qc etc from FileName.txt
        if not os.path.exists(self.dataInfoFile):
            return False
        
        # If is a file, then read it 
        if not self.searchDir:
            with open(self.dataInfoFile, 'r') as fhIn:
                for line in fhIn:
                    if line[0] == '#':
                        flag = line.lstrip('#').rstrip()
                        continue
                    self.dataInfoDict.setdefault(flag, []).append(line.strip())
            return self.dataInfoDict
        else:
            self.dataInfoDict = self.constructInfoDict(os.path.dirname(self.dataInfoFile))
            return self.dataInfoDict

    def constructInfoDict(self, intensityDir):
        infoDict = {}
        resultDir = os.path.dirname(intensityDir)
        infoDict['fq'] = sorted(glob.glob(os.path.join(resultDir, '*.fq.gz')))

        finIntPath = os.path.join(intensityDir, 'finInts')
        lastCyclePath = sorted(glob.glob(os.path.join(finIntPath, 'S*')))[-1]
        infoDict['QC'] = sorted(glob.glob(os.path.join(lastCyclePath, 'C*R*.QC.txt')))

        
        barcodeStatFile = os.path.join(intensityDir, 'BarcodeStat.txt')
        sequenceStatFile = os.path.join(intensityDir, 'SequenceStat.txt')
        for f in [barcodeStatFile, sequenceStatFile]:
            if os.path.exists(f):
                infoDict[os.path.basename(f).strip('.')[0]] = [f]

        return infoDict

    def getLaneInfo(self):
        # Get info about lane like readLength etc from Input_xx.txt
        if os.path.exists(self.laneInfoFile):
            with open(self.laneInfoFile, 'r') as fhIn:
                for line in fhIn:
                    line = line.strip().split()
                    self.laneInfoDict[line[0]] = line[1]
            return self.laneInfoDict
        else:
            print 'Cant not find %s.' % (self.laneInfoFile)
            return False

    def prepareData(self, resultAddr):
        qcFileList = self.dataInfoDict['QC']
        qcString = ''

        for qcFile in qcFileList:
            if os.path.exists(qcFile):
                name = os.path.basename(qcFile).split('.')[0]
                with open(qcFile, 'r') as fhIn:
                    qcString += '#FOV\n%s\n' % (name)
                    qcString += fhIn.read()

        fhOut = open(os.path.join(resultAddr, 'BasecallQC.txt'), 'w')
        fhOut.write(qcString)
        fhOut.close()
      
        if 'BarcodeStat' in self.dataInfoDict and 'SequenceStat' in self.dataInfoDict:
            barcodeFile = self.dataInfoDict['BarcodeStat'][0]
            seqStatFile = self.dataInfoDict['SequenceStat'][0]
            for f in [barcodeFile, seqStatFile]:
                if os.path.exists(f):
                    shutil.copy(f, resultAddr)

        # Copy FileName.txt to cluster
        try:
            shutil.copy(self.dataInfoFile, resultAddr)
        except:
            pass

    def parseParameter(self):
        # Parse parameters which be useful to following analysis
        self.uploadFq = self.configDict['uploadFq']
        self.uploadImage = self.configDict['uploadImage']
        self.uploadPerformance = False if self.configDict['uploadPerformance'] == 'False' else True
        self.mapping = self.configDict['mapping']
        self.imageFov = self.configDict['imageFov']

        self.read1_length = self.laneInfoDict['read1_length']
        self.read2_length = self.laneInfoDict['read2_length']
        self.rawPath = self.laneInfoDict['output']
        if self.rawPath[-1] == '\\':
            self.rawPath = self.rawPath[:-8]
        else:
            self.rawPath = self.rawPath[:-7]

        fqList = self.dataInfoDict['fq']
        if len(fqList) <= 0:
            print "There is not fq."
            return False

        self.copyedDir = os.path.dirname(fqList[0])    
        self.destDir = self.configDict['address']
        self.infoDir = self.configDict['infoAddress']  
        self.directoryPermission = self.configDict['directoryPermission']
        self.filePermission = self.configDict['filePermission']
        self.transferStatus = 'Normal'

        if self.mapping != 'True':
            self.transferStatus = 'NoMapping'

        self.lane = os.path.basename(self.copyedDir)
        self.slide = os.path.basename(os.path.dirname(self.copyedDir))
        self.select = self.configDict['select']

        ### para  config  final
        ###  T      T       T
        ###  T      F       F
        ###  F      F       F
        ###  F      T       F
        if not ((self.uploadFq == 'True') and self.upload):
            print 'No upload.'
            return False

        # Judgement the read's length, then determine upload or not
        if int(self.read2_length) <= 0 and int(self.read1_length) < self.minUploadCycle:
            print "Cycle number is %d, less than %d, so don't upload." % (int(self.read1_length), self.minUploadCycle)
            return False

        return True


    def processUploadImage(self):
        # Process images to upload
        if self.uploadImage == 'True':
            self.imagePath = self.rawPath + '\\' + self.lane
            try:
                self.resultTarImage = self.prepareImage(self.copyedDir)
            except Exception, e:
                print e
                print 'Upload images failed!'
                self.resultTarImage = None

    def processUploadPerformance(self):
        # Process directory of performance to upload
        if not self.uploadPerformance:
		    return
        self.performancePath = self.rawPath + '\\' + 'Performance'
        if os.path.exists(self.performancePath):
            self.uploadPerformance = True
        else:
            self.uploadPerformance = False

    def transformAddr(self, addr):
        return "/cygdrive/%s/%s/" % (addr[0], addr[3:])

    def prepareImage(self, copyedDir):
        if self.imagePath is None or not os.path.exists(self.imagePath):
            raise Exception, "No image path of %s." % (self.imagePath)
        
        # 1. Copy images according to the specified fovs
        fovList = self.imageFov
        resultDir = os.path.join(copyedDir, 'rawImage')
        resultTarFile = os.path.join(copyedDir, 'rawImage.tar')
        hasImages = archiveImage(self.imagePath, resultDir, fovList)

        # 2. Compressed images directory
        currentDir = os.path.abspath(os.path.dirname(__file__))
        tar = os.path.join(currentDir, 'software', 'tar-1.13', 'bin', 'tar.exe')
        try:
            if hasImages:
                os.chdir(copyedDir)
                result = subprocess.check_output(r'%s -cf %s %s' % (tar, 'rawImage.tar', 'rawImage'), shell=True, stderr=subprocess.STDOUT)
        except Exception, e:
            raise e
        finally:
            shutil.rmtree(resultDir)

        return resultTarFile

    def uploadRemoteData(self):
        # 1. Prepare data for copy
        remoteHost = self.configDict['host']
        resultDir = self.destDir + self.slide + '/' + self.lane  + '/'

        # 2. Construct Dir for zebraName
        os.popen('%s %s "mkdir -p %s"' % (self.ssh, remoteHost, resultDir))

        # 3. Copy data Dir to destDir
        os.popen(r'%s %s %s %s:%s' % (self.rsync, self.rsyncPara, self.transformAddr(self.copyedDir).replace('\\', '/'), \
                         remoteHost, resultDir))
        # 4. Chmod of the dir
        os.popen('%s %s "find %s -type f -exec chmod %s {} \; \
                    && find %s -type d -exec chmod %s {} \;"' \
                    % (self.ssh, remoteHost, resultDir, self.filePermission, resultDir, self.directoryPermission))

        # 5. Upload performance
        if self.uploadPerformance:
            resultPerformancePath = self.destDir + self.slide
            os.popen(r'%s %s %s %s:%s' % (self.rsync, self.rsyncPara, self.transformAddr(self.performancePath)[:-1].replace('\\', '/'), \
                             remoteHost, resultPerformancePath))
            os.popen('%s %s "find %s -type f -exec chmod %s {} \; \
                        && find %s -type d -exec chmod %s {} \;"' \
                        % (self.ssh, remoteHost, resultPerformancePath+'/'+'Performance', self.filePermission, resultPerformancePath+'/'+'Performance', self.directoryPermission))

    def uploadLocalData(self):
        # 1. Prepare data for copy
        resultDir = self.destDir + self.slide + '\\' + self.lane 

        # 2. Construct Dir for zebraName
        os.popen(r'mkdir %s' % (resultDir))

        # 3. Copy data Dir to destDir
        os.popen(r'%s %s %s %s' % (self.rsync, self.rsyncPara, self.transformAddr(self.copyedDir).replace('\\', '/'),
                                self.transformAddr(resultDir).replace('\\', '/')))

        # 4. Upload performance
        if self.uploadPerformance:
            resultPerformancePath = self.destDir + self.slide
            os.popen(r'%s %s %s %s' % (self.rsync, self.rsyncPara, self.transformAddr(self.performancePath)[:-1].replace('\\', '/'), \
                    self.transformAddr(resultPerformancePath).replace('\\', '/')))

    def uploadInfo(self):
        option = ['zebra_name', 'slide_id', 'lane_id', 'reference', 'seq_type', 'current_time', 'transferStatus']

        self.uploadPara['zebra_name'] = self.configDict['name'] if 'name' in self.configDict else 'Zebra00'
        self.uploadPara['slide_id'] = self.slide
        self.uploadPara['lane_id'] = self.lane
        if self.lane[-1] == '1':
            self.uploadPara['reference'] = self.laneInfoDict['reference']
        elif self.lane[-1] == '2':
            self.uploadPara['reference'] = self.laneInfoDict['reference2']
        self.uploadPara['seq_type'] = 'SE' if int(self.read1_length) == 0 or int(self.read2_length) == 0 else 'PE'
        self.uploadPara['transferStatus'] = self.transferStatus
        self.uploadPara['current_time'] = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        uploadString = '\t'.join(self.uploadPara[k] for k in option)

        print uploadString

        if self.select == 'remote':
            remoteHost = self.configDict['host']
            os.popen('%s %s "echo %s >> %s" ' % (self.ssh, remoteHost, uploadString, \
                            os.path.join(self.infoDir, '%s_%s.txt' % (self.slide, self.lane))))
        else:
            os.popen('echo %s >> %s' % (uploadString, os.path.join(self.infoDir, '%s_%s.txt' % (self.slide, self.lane)))) 

    def copyData(self):
        # 1. Parse parameters from files
        if self.readConfig() and self.getDataInfo() and self.getLaneInfo():
            if not self.parseParameter():
                return
        else:
            return

        # 2. Process extra data to uploaded
        self.processUploadImage()
        self.processUploadPerformance()

        # 3. Start upload data to cluster
        self.prepareData(self.copyedDir)
        try:
            if self.select == 'remote':
                self.uploadRemoteData()
            elif self.select == 'local':
                self.uploadLocalData()
            else:
                raise Exception, "Select Error, %s." % self.select
        except Exception as e:
            print 'Load failed.'
            self.transferStatus = 'Failed'

        # 4. Add lane's info to seqDataInfo.txt
        self.uploadInfo()

        # 5. Remove images after upload
        if self.uploadImage == 'True' and self.resultTarImage is not None and os.path.exists(self.resultTarImage):
            p = self.resultTarImage
            try:
                if os.path.isfile(p):
                    os.remove(p)
                elif os.path.isdir(p):
                    shutil.rmtree(p)
            except:
                pass

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################


#################################
##
##   Main function of program.
##
#################################
def Main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    #ArgParser.add_argument("-i", "--imagePath", action="store", dest="imagePath", default=None, help="Input images path to upload. [%(default)s]")
    ArgParser.add_argument("-n", "--notUpload", action="store_false", dest="upload", default=True, help="Is upload data to cluster. [%(default)s]")
    ArgParser.add_argument("-s", "--searchDir", action="store_true", dest="searchDir", default=False, help="If data was copied to other dir, It will search dir for search fq and etc. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "ERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (dataInfoFile, configFile, laneInfoFile) = args

    ############################# Main Body #############################
    startTime = datetime.datetime.now()
    print 'Upload Start Time: %s\n' % startTime.strftime("%Y-%m-%d %H:%M:%S")

    copyData = CopyData(dataInfoFile, configFile, laneInfoFile, para.upload, para.searchDir)
    copyData.copyData()

    endTime = datetime.datetime.now()
    print 'Upload End Time: %s\n' % endTime.strftime("%Y-%m-%d %H:%M:%S")
    print 'Upload result spend time %s\n' % (endTime - startTime)
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    Main()


