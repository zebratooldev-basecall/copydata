#!/bin/bash

cur_date=$(date "+%Y%m%d%H%M")
raw_dir=$cur_date

if [ -e $raw_dir ];
then
    rm -rf $raw_dir
fi

mkdir -p $raw_dir

#cp -R software $raw_dir
#cp archiveImage.py copyData.py $raw_dir
file_list=('software' 'copyData.py' 'archiveImage.py')
for f in ${file_list[*]}; do
    cp -R $f $raw_dir
done
